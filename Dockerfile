FROM debian:bookworm

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && \
    apt-get install --no-install-recommends -y -q ca-certificates curl git jq wget

RUN curl -L -s https://gitlab.com/greg/scripts/-/raw/master/glab-install.sh | bash
